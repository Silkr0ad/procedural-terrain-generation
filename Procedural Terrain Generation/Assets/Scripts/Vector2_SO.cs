﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Vector2", menuName = "ScriptableObjects/Vector 2", order = 1)]
public class Vector2_SO : ScriptableObject {
    public Vector2 vec2;
}
