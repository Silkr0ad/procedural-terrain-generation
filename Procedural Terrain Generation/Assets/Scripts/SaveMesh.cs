﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MeshGenerator))]
public class SaveMesh : Editor {
    string savePath;

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        MeshGenerator mg = (MeshGenerator)target;

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Save Mesh as Asset")) {
            savePath = EditorUtility.SaveFilePanelInProject("Save Mesh", "Terrain", "asset", "What message?");
            AssetDatabase.DeleteAsset(savePath);
            AssetDatabase.CreateAsset(mg.mesh, savePath);
            AssetDatabase.SaveAssets();
            mg.SavePosition();
        }

        if (GUILayout.Button("Adjust Position")) {
            mg.AdjustToSavedPosition();
        }
        
        GUILayout.EndHorizontal();
        
    }
}
