﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class MeshGenerator : MonoBehaviour {
    public int xSize = 16;
    public int zSize = 16;
    public float resolution = 1f;
    [Range(1, 5)] public int octaves = 3;
    [Range(0f, 1f)] public float persistence = 0.5f;
    [Range(0f, 0.2f)] public float lacunarity = 2f;
    public bool update = false;
    
    [HideInInspector] public Mesh mesh;

    Vector3[] vertices;
    int[] triangles;
    Vector2[] uv;

    public Vector2_SO savedPosition;

    private void Awake() {
        // create a new (empty) mesh
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
    }

    private void Start() {
        // create the terrain and save the mesh
        CreateShape();
        UpdateMesh();
    }

    private void Update() {
        if (update) {
            // move the transform to the center
            AdjustPosition();

            // add basic terrain and update the mesh
            CreateShape();
            UpdateMesh();
        }
        
        #region Scale and Lacunarity Adjustment | FOR TWEAKING AND EXPERIMENTING AND DEBUGGING
        // add ability to change scale and lacunarity in-game | FOR DEBUGGING PURPOSES
        if (Input.GetKey(KeyCode.DownArrow)) {
            lacunarity -= 1 * Time.deltaTime;
        } else if (Input.GetKey(KeyCode.UpArrow)) {
            lacunarity += 1 * Time.deltaTime;
        }
        #endregion
    }

    private void CreateShape() {
        vertices = new Vector3[(xSize + 1) * (zSize + 1)];
        triangles = new int[(xSize * zSize * 6)];
        uv = new Vector2[(xSize + 1) * (zSize + 1)];


        #region Generate the Vertices
        for (int i = 0, z = 0; z < zSize + 1; z++) {
            for (int x = 0; x < xSize + 1; x++) {
                // generate the vertex
                float frequency = 1f, amplitude = 1f, y = 0f;
                for (int octave = 0; octave < octaves; octave++) {
                    y += (Mathf.PerlinNoise(x * frequency, z * frequency) * 2 - 1) * amplitude; // "* 2 - 1" is to keep it above-ground
                    frequency *= lacunarity;                                                    // (remember, PerlinNoise always returns 0..1)
                    amplitude *= persistence;
                }

                vertices[i++] = new Vector3(x / resolution, y, z / resolution);
            }
        }
        #endregion

        #region Generate the Triangles
        int vert = 0;
        int tris = 0;

        for (int z = 0; z < zSize; z++) {
            for (int x = 0; x < xSize; x++) {
                triangles[tris + 0] = vert;
                triangles[tris + 1] = vert + xSize + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + xSize + 1;
                triangles[tris + 5] = vert + xSize + 2;

                vert += 1;
                tris += 6;
            }
            vert += 1;
        }
        #endregion

        #region Generate the UVs
        for (int i = 0, z = 0; z < zSize + 1; z++) {
            for (int x = 0; x < xSize + 1; x++) {
                uv[i++] = new Vector2((float)x / xSize, (float)z / zSize);
            }
        }
        #endregion
    }

    private void UpdateMesh() {
        Mesh mesh = this.mesh;
        mesh.Clear();

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uv;
        mesh.RecalculateNormals();
    }

    private void AdjustPosition() {
        transform.position = new Vector3(-xSize / 2f / resolution, 0f, -zSize / 2f / resolution);
    }

    public void AdjustToSavedPosition() {
        transform.position = new Vector3(savedPosition.vec2.x, 0f, savedPosition.vec2.y);
    }

    public void SavePosition() {
        savedPosition.vec2 = new Vector2(transform.position.x, transform.position.z);
    }

    private void OnDrawGizmosSelected() {
        if (vertices == null)
            return;
        for (int i = 0; i < vertices.Length; i++) {
            Gizmos.DrawSphere(vertices[i], 0.2f);
        }
    }
}
