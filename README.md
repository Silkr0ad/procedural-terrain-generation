# Procedural Terrain Generation

My attempt at creating 3D terrain procedurally. The project is based on Perlin noise and uses the CPU.

![Showcase GIF](Media/proc_gen.gif)

## Branches

Didn't use git for this one 🙄 so only master. No commit history, either. 

## Status

### Half-Complete

It works just fine, but only up to a vertex size of 255x255. This is near the maximum number of vertices Unity allows per 1 mesh. The limit can be lifted, but it's not great for performance; much better to divide it into chunks. This is the part that needs completion, but I've scratched my itch for now, so I'll come back to finish it when I actually need it in another project.

Another note: could be converted to the GPU for much better performance. But it works perfectly fine as it is.

#### Update

Upgrading the project to a newer version of Unity has broken the Inspector GUI code. Gives a weird error message. Everything works perfectly fine, though. ¯\_(ツ)_/¯

## Attributions

Thanks, Brackeys!